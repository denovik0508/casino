# casino

## API

### Chips buying

#### Request
 
`GET /buyChips?userId=?&amount=?`

#### Result (Content-Type: application/json)

* **OK**: HTTP 200

```json
{
    "amount": 2
}
```

* **Error**: HTTP 422 + error text

### Getting chips amount

#### Request

`GET /chipsAmount?userId=?`

#### Result (Content-Type: application/json)

* **OK**: HTTP 200

```json
{
    "amount": 0
}
```

### Do game

#### Request

`GET /game?userId={userId}&bet={bet}&chips={chips}`

#### Result (Content-Type: application/json)

* **Win**: HTTP 200

```json
{
    "status": "win"
}
```

* **Lose**: HTTP 200

```json
{
    "status": "lose"
}
```

* **Error**: HTTP 422 + error text
