package com.denovik.casino.service;

public class GameStub implements Game {

    public int winBet;

    @Override
    public int getWinBet() {
        return winBet;
    }

    public GameStub setWinBet(int winBet) {
        this.winBet = winBet;
        return this;
    }
}
