package com.denovik.casino.service;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class GameServiceTest {

    private GameService gameService = new GameService();

    @Test
    public void playGame_whenWin_then() {
        int userId = 1;
        int bet = 1;
        int chips = 1;
        gameService.setGame(new GameStub().setWinBet(1));

        boolean result = gameService.playGame(userId, bet, chips);

        assertThat(result, equalTo(true));
    }

    @Test
    public void playGame_whenLoose_then() {
        int userId = 1;
        int bet = 1;
        int chips = 1;
        gameService.setGame(new GameStub().setWinBet(2));

        boolean result = gameService.playGame(userId, bet, chips);

        assertThat(result, equalTo(false));
    }
}
