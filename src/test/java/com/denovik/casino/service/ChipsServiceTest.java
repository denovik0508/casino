package com.denovik.casino.service;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ChipsServiceTest {

    private ChipsService chipsService = new ChipsService();

    @Test
    public void buyChips_whenUserHasNoChips_thenReturnBought() {
        int userId = 1;

        int amount = chipsService.buyChips(userId, 1);

        assertThat(amount, equalTo(1));
    }

    @Test
    public void buyChips_whenUserHasChips_thenReturnTotalAmount() {
        int userId = 1;
        chipsService.buyChips(userId, 1);

        int amount = chipsService.buyChips(userId, 1);

        assertThat(amount, equalTo(2));
    }

    @Test
    public void chipsAmount_whenUserHasNoChips_thenReturnZero() {
        int userId = 1;

        int amount = chipsService.chipsAmount(userId);

        assertThat(amount, equalTo(0));
    }

    @Test
    public void chipsAmount_whenUserHasChips_thenReturnTotalAmount() {
        int userId = 1;
        chipsService.buyChips(userId, 1);

        int amount = chipsService.chipsAmount(userId);

        assertThat(amount, equalTo(1));
    }
}
