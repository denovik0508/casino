package com.denovik.casino.controller;

import com.denovik.casino.result.BuyChipsResult;
import com.denovik.casino.result.ChipsAmountResult;
import com.denovik.casino.service.ChipsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ChipsController {

    private final ChipsService chipsService;

    @GetMapping("/chipsAmount")
    private ResponseEntity<ChipsAmountResult> chipsAmount(@RequestParam int userId) {
        return ResponseEntity.ok(new ChipsAmountResult(
                chipsService.chipsAmount(userId)
        ));
    }

    @GetMapping("/buyChips")
    private ResponseEntity<BuyChipsResult> buyChips(@RequestParam int userId, @RequestParam int amount) {
        return ResponseEntity.ok(new BuyChipsResult(
                chipsService.buyChips(userId, amount)
        ));
    }
}
