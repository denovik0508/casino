package com.denovik.casino.controller;

import com.denovik.casino.result.PlayResult;
import com.denovik.casino.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class GameController {

    private final GameService gameService;

    @GetMapping("/game")
    private ResponseEntity<PlayResult> playGame(
            @RequestParam int userId,
            @RequestParam int bet,
            @RequestParam int chips
    ) {
        return ResponseEntity.ok(new PlayResult(
                gameService.playGame(userId, bet, chips)
        ));
    }
}
