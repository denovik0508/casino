package com.denovik.casino.service;

import java.util.Random;

public class RandomGame implements Game {
    @Override
    public int getWinBet() {
        return new Random().nextInt(6) + 1;
    }
}
