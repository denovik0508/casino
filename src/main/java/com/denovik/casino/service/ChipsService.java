package com.denovik.casino.service;

import com.denovik.casino.entity.User;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ChipsService {

    private Map<User, Integer> userChips = new HashMap<>();

    public int buyChips(int userId, int buyChipsAmount) {
        User user = new User().setId(userId);
        Integer amount = userChips.get(user);
        if (amount == null) {
            amount = buyChipsAmount;
        } else {
            amount += buyChipsAmount;
        }
        userChips.put(user, amount);
        return amount;
    }

    public int chipsAmount(int userId) {
        User user = new User().setId(userId);
        Integer amount = userChips.get(user);
        return amount == null ? 0 : amount;
    }
}
