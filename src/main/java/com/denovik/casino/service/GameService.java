package com.denovik.casino.service;

import org.springframework.stereotype.Service;

@Service
public class GameService {

    private Game game = new RandomGame();

    public boolean playGame(int userId, int bet, int chips) {
        return bet == game.getWinBet();
    }

    public void setGame(Game game) {
        this.game = game;
    }
}
