package com.denovik.casino.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class ChipsAmountResult {
    private final int amount;
}
