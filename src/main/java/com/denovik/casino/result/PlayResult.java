package com.denovik.casino.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class PlayResult {
    private final String status;

    public PlayResult(boolean status) {
        this.status = status ? "win" : "loose";
    }
}
