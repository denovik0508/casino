from behave import fixture, use_fixture
from random import randint
import os


def before_all(context):
    api_endpoint = os.getenv('API_ENDPOINT', 'localhost:8085')
    context.api_endpoint = f'http://{api_endpoint}/'


def before_scenario(context, scenario):
    context.user_id = str(randint(0, 999999))
