# Created by mnabiulin at 21.10.2020
Feature: Testing of game

  Scenario: Buy minimal amount of chips
    Given user without chips
    When user buys "1" chips
    Then user will have "1" chips


  Scenario: Buy incorrect amount of chips
    Given user without chips
    When user buys "-1" chips
    Then user will get an error message
    And amount of chips won't changed


  Scenario: Buy additional chips
    Given user with "1" chips
    When user buys additional "1" chips
    Then user will have "2" chips


  Scenario: Buy additional chips with incorrect value
    Given user with "1" chips
    When user buys additional "-1" chips
    Then user will get an error message
    And amount of chips won't changed

#  Scenario: Start game
#  """
#    Сценарий описывает игру пользователя, в которой можно как выиграть, так и проиграть, по-этому проверка не
#    описывает конкретное кол-во фишек у пользователя после игры
#    """
#    Given user with "1" chips
#    When user starts game with "1" chips
#    Then user will have correct chips amount
#
#
#  Scenario: Start game using more chips than user have
#  """
#    Сценарий описывает игру пользователя, в которой можно как выиграть, так и проиграть, по-этому проверка не
#    описывает конкретное кол-во фишек у пользователя после игры
#    """
#    Given user with "1" chips
#    When user starts game with "2" chips
#    Then user will get an error message
#    And user will have the same chips amount
#
#  Scenario: Start game using incorrect chips amount
#  """
#    Сценарий описывает игру пользователя, в которой можно как выиграть, так и проиграть, по-этому проверка не
#    описывает конкретное кол-во фишек у пользователя после игры
#    """
#    Given user with "1" chips
#    When user starts game with "-1" chips
#    Then user will get an error message
#    And user will have the same chips amount



