from behave import *
import requests


@given("user without chips")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    pass


@when('user buys "{chips_amount}" chips')
def step_impl(context, chips_amount):
    """
    :param chips_amount: amount of chips for user
    :type context: behave.runner.Context
    """
    context.result = buy_chips(context.api_endpoint, context.user_id, chips_amount)


@then('user will have "{chips_amount}" chips')
def step_impl(context, chips_amount):
    """
    :param chips_amount: amount of chips for user
    :type context: behave.runner.Context
    """
    chips = chips_status(context.api_endpoint, context.user_id)
    assert context.result.status_code == 200
    assert chips.json()['amount'] == str(chips_amount)


@then("user will get an error message")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert context.result.status_code == 422


@step("amount of chips won't changed")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    pass


@given('user with "{chips_amount}" chips')
def step_impl(context, chips_amount):
    """
    :param chips_amount: amount of chips for user
    :type context: behave.runner.Context
    """
    pass


@when('user buys additional "{chips_amount}" chips')
def step_impl(context, chips_amount):
    """
    :param chips_amount: amount of chips for user
    :type context: behave.runner.Context
    """
    pass


@when('user starts game with "{chips_amount}" chips')
def step_impl(context, chips_amount):
    """
    :param chips_amount:
    :type context: behave.runner.Context
    """
    pass


@then("user will have correct chips amount")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    pass


@step("user will have the same chips amount")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    pass


def buy_chips(api_endpoint, user_id, chips_amount):
    url = f'{api_endpoint}buyChips'
    payload = {'userId': user_id, 'amount': chips_amount}
    return requests.get(url, params=payload)


def chips_status(api_endpoint, user_id):
    url = f'{api_endpoint}chipsAmount'
    payload = {'userId': user_id}
    return requests.get(url, params=payload)


# def start_game(api_endpoint, user_id, bet_amount):
#     url = f'{api_endpoint}game'
#     payload = {'userId': user_id, 'bet': chips_amount}
#     return requests.get(url, params=payload)
