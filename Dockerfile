FROM gradle:6.6.1-jdk11 as compiler
WORKDIR /src
COPY . .
RUN gradle build -x test
RUN find /src -name '*.jar'

FROM openjdk:11-jre-slim
COPY --from=compiler /src/build/libs/src-0.0.1.jar /casino.jar

EXPOSE 8085
CMD ["java", "-jar", "/casino.jar"]
